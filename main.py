import json
import numpy as np
from flask import Flask, request

from werkzeug.contrib.fixers import ProxyFix

import pickle

gbm = pickle.load(open('models/lgbm_model.pkl', 'rb'))
count_vectorizer = pickle.load(open('models/count_vec.pkl', 'rb'))
tfidf = pickle.load(open('models/tfidf.pkl', 'rb'))

classes = ['Boredom', 'Fear', 'Greeting', 'Humor', 'Indecency', 'Love',
           'Rudeness', 'Sarcasm', 'Slang', 'Surprise', 'Undefined']


def softmax(x):
    return np.exp(x) / np.sum(np.exp(x))


app = Flask(__name__)

app.wsgi_app = ProxyFix(app.wsgi_app)


@app.route('/predict', methods=['GET'])
def predict():
    sent = request.form.get('sentence')
    if sent is None:
        sent = ''

    pred = softmax(gbm.predict(tfidf.transform(count_vectorizer.transform([sent])), num_iteration=gbm.best_iteration))
    top_n = np.argsort(np.array(pred[0]))[len(classes) - 3:]

    result = {}
    for i in np.flip(top_n, axis=0):
        result[classes[i]] = int(pred[0][i] * 10000) / 10000.0

    print(sent, pred, result)

    return json.dumps(result)
