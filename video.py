import argparse
import os
import pickle
from build_vocab import Vocabulary
import cv2
import dlib
import numpy as np
import os.path as osp
import pandas as pd
import torch
from torch.autograd import Variable
from torchvision import transforms
from tqdm import *

from data import VOC_CLASSES as labels
from model import EncoderCNN, DecoderRNN
from ssd import build_ssd


def init(use_gpu):
    device = 0

    net = build_ssd('test', 300, 21)  # initialize SSD
    net.load_weights('models/ssd300_mAP_77.43_v2.pth')
    if use_gpu:
        net = net.to(device)

    # Load vocabulary wrapper
    with open(vocab_path, 'rb') as voc_file:
        vocab = pickle.load(voc_file)


    encoder = EncoderCNN(embed_size).eval()  # eval mode (batchnorm uses moving mean/variance)
    decoder = DecoderRNN(embed_size, hidden_size, len(vocab), num_layers)
    if use_gpu:
        encoder = encoder.to(device)
        decoder = decoder.to(device)

    # Load the trained model parameters
    encoder.load_state_dict(torch.load(encoder_path))
    decoder.load_state_dict(torch.load(decoder_path))

    return net.eval(), encoder.eval(), decoder.eval(), vocab

vocab_path = 'models/vocab.pkl'
embed_size = 256
hidden_size = 512
num_layers = 1
encoder_path = 'models/encoder-5-3000.pkl'
decoder_path = 'models/decoder-5-3000.pkl'

detector = dlib.get_frontal_face_detector()
sp = dlib.shape_predictor('models/shape_predictor_5_face_landmarks.dat')
facerec = dlib.face_recognition_model_v1('models/dlib_face_recognition_resnet_model_v1.dat')

transform = transforms.Compose([
    transforms.Normalize((0.485, 0.456, 0.406),
                         (0.229, 0.224, 0.225))])


race_model = pickle.load(open('models/race_utk.pkl', 'rb'))
gender_model = pickle.load(open('models/male_female_svm.pkl', 'rb'))


gbm = pickle.load(open('models/lgbm_model.pkl', 'rb'))
count_vectorizer = pickle.load(open('models/count_vec.pkl', 'rb'))
tfidf = pickle.load(open('models/tfidf.pkl', 'rb'))

classes = ['Boredom', 'Fear', 'Greeting', 'Humor', 'Indecency', 'Love',
           'Rudeness', 'Sarcasm', 'Slang', 'Surprise', 'Undefined']

gender2labels = {-1:'undefinied',0:'woman',1:'man'}
race2labels = {0:'white',1:'black',2:'asian', 3:'indian',4:'other',-1:'undefinied'}


def get_colors_by_path(path):
    cap = cv2.VideoCapture(path)
    ret, frame = cap.read()
    return get_colors(frame)


def get_colors(frame):
    clrs = np.histogram(frame[:, :, 0]), np.histogram(frame[:, :, 1]), np.histogram(frame[:, :, 2])
    return np.argmax(clrs[0][0][1:]) + 1, np.argmax(clrs[1][0][1:]) + 1, np.argmax(clrs[2][0][1:]) + 1


def get_race_gender_by_path(path):
    cap = cv2.VideoCapture(path)
    ret, frame = cap.read()
    return get_race_gender(frame)


def get_race_gender(frame):
    img = cv2.cvtColor((frame), cv2.COLOR_BGR2RGB)

    dets = detector(img, 1)
    if len(dets) == 0:
        return -1, -1
    shape = sp(img, dets[0])
    desc = np.array(facerec.compute_face_descriptor(img, shape)).reshape((1, 128))

    return race2labels[race_model.predict(desc)[0]], gender2labels[gender_model.predict(desc)[0]]


def get_dynamic_by_path(path):
    return get_dynamic(cv2.VideoCapture(path))


def get_dynamic(frames):
    max_norm = 0
    norms = []

    for i in range(len(frames) - 1):
        norm = np.linalg.norm(frames[i] - frames[i + 1])
        norms.append(norm)
        if norm > max_norm:
            max_norm = norm

    norms = np.array(norms)

    return pow(np.mean(norms / max_norm), 2)


def softmax(x):
    return np.exp(x) / np.sum(np.exp(x))


def predict(text):
    pred = softmax(gbm.predict(tfidf.transform(count_vectorizer.transform([text])), num_iteration=gbm.best_iteration))
    top_n = np.argsort(np.array(pred[0]))[len(classes) - 3:]

    result = {}
    for i in np.flip(top_n, axis=0):
        result[classes[i]] = int(pred[0][i] * 10000) / 10000.0
    return result


def ocv2torch(img):
    '''
    bgr opencv image
    :param img:
    :return:
    '''
    return torch.from_numpy(img[:, :, (2, 1, 0)].astype(np.float32)).permute(2, 0, 1)


def get_top3_emo_by_path(path):
    return get_top3_emo(cv2.VideoCapture(path).read()[1])


def get_top3_emo(image):
    with torch.no_grad():
        frame = transform(ocv2torch(cv2.resize(image, (224, 224)))).unsqueeze(0)
        if USE_GPU:
            image_tensor = frame.to(0)
        else:
            image_tensor = frame

    # Generate an caption from the image
    feature = encoder(image_tensor)
    sampled_ids = decoder.sample(feature)
    sentences = []
    # sampled_ids = sampled_ids.cpu().numpy()
    ids = sampled_ids[0]
    ids = ids.cpu().numpy()
    # Convert word_ids to words
    sampled_caption = []
    for word_id in ids:
        word = vocab.idx2word[word_id]
        sampled_caption.append(word)
        if word == '<end>':
            break

    sentence = ' '.join(sampled_caption).replace('<start>','').replace('<end>','')

    frame.detach()

    return predict(sentence), sentence


def get_objects_by_path(path):
    image = cv2.VideoCapture(path).read()[1]
    return get_objects(image)


def get_objects(image):
    x = cv2.resize(image, (300, 300)).astype(np.float32)
    x -= (104.0, 117.0, 123.0)
    x = x.astype(np.float32)
    x = x[:, :, ::-1].copy()
    x = torch.from_numpy(x).permute(2, 0, 1)

    with torch.no_grad():
        xx = Variable(x.unsqueeze(0))  # wrap tensor in Variable
        if USE_GPU:
            xx = xx.to(0)
        y = net(xx)

    detections = y.detach()
    # scale each detection back up to the image
    scale = torch.Tensor(image.shape[1::-1]).repeat(2)

    result = []

    for i in range(detections.size(1)):
        j = 0
        while detections[0, i, j, 0] >= 0.6:
            score = detections[0, i, j, 0]
            label_name = labels[i - 1]
            display_txt = '%s: %.2f' % (label_name, score)
            pt = (detections[0, i, j, 1:] * scale).cpu().numpy()
            coords = (pt[0], pt[1]), pt[2] - pt[0] + 1, pt[3] - pt[1] + 1
            result.append((label_name, score.cpu().numpy(), coords))
            j += 1

    scale.detach()
    xx.detach()
    return result


def parse_video(path):
    cap = cv2.VideoCapture(path)

    frames = []

    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break
        else:
            frames.append(frame)

    emo, desc = get_top3_emo(frames[0])
    race, gender = get_race_gender(frames[0])
    return emo, desc, race, gender, get_dynamic(frames), get_colors(frames[0]), get_objects(frames[0])


def print_output(output, args):

    type = args.output
    csv_path = args.csv_path

    if type == 'console':
        for k in output.keys():
            print(k, output[k])
    elif type == 'dump':
        pickle.dump(output, open('dump.pkl', 'wb'))
    elif type == 'csv':
        df = pd.DataFrame(columns=['vid', 'emo', 'description', 'race', 'gender', 'dynamics', 'color', 'detection'])
        for k in output.keys():
            apnd = [k]
            apnd.extend(output[k])
            df = df.append(pd.Series(apnd, index=['vid', 'emo', 'description', 'race', 'gender', 'dynamics', 'color', 'detection']), ignore_index=True)

        if csv_path is not None:
            print('writing to',csv_path)
            df.to_csv(csv_path, index=False)
        else:
            print('writing to output.csv')
            df.to_csv('output.csv',index=False)


parser = argparse.ArgumentParser()
parser.add_argument('--video', type=str, help='Single video path to be processes')
parser.add_argument('--video_path', type=str, help='Path to directory with target videos')
parser.add_argument('--output', default='console', type=str, choices=['console', 'dump', 'csv'], help='Output type, could be console output, raw pickle dump or csv')
parser.add_argument('--csv_path', type=str,help='Path to write output csv in')
parser.add_argument('--device',type=str, choices=['gpu','cpu'], default='cpu', help='gpu option can speed up object detection')

if __name__ == '__main__':
    args = parser.parse_args()
    video_path = args.video_path
    video = args.video

    if args.device=='cpu' or not torch.cuda.is_available():
        USE_GPU=False
        torch.set_default_tensor_type('torch.FloatTensor')
    elif args.device == 'gpu':
        USE_GPU = True
        torch.set_default_tensor_type('torch.cuda.FloatTensor')

    global net, encoder, decoder, vocab
    net, encoder, decoder, vocab = init(USE_GPU)

    output = {}

    if video_path is not None:
        for f in tqdm(os.listdir(video_path)):
            vid_path = osp.join(video_path, f)
            idx = vid_path[vid_path.rfind('/') + 1:len(vid_path) - 4]
            output[idx] = parse_video(vid_path)
    elif video is not None:
        idx = video[video.rfind('/') + 1:len(video) - 4]
        output[idx] = parse_video(video)
    else:
        print('Please specify video_path or video to process')

    print_output(output, args)
