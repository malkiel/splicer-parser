Build docker image and run it.
Docker image contains text emotion recognition service
```bash
docker build -t splicer .
docker run -p 80:80 -d -t splicer
```

Request example:

```bash
curl -X GET -F sentence='hi there' http://127.0.0.1:80/predict
```

Response:
```bash
{"Greeting": 0.0898, "Undefined": 0.1208, "Humor": 0.0905}
```

To perform video parsing call the following script:
```bash
python video.py --video_path=/mnt/dataset/test/ --output=csv --csv_path=test/out.csv
```

In this example, script parses all videos from /mnt/dataset/test and produces output to test/out.csv.
Directory test should be created before calling script.

Only python3 is supported.