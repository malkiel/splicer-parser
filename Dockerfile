FROM ubuntu:16.04

MAINTAINER Artyom Lyan 'avn8068@gmail.com'

RUN apt-get update -y && apt-get install -y apt-utils python3-pip python3-dev build-essential git libgtk2.0-dev supervisor nginx cmake

COPY . /app
WORKDIR /app

RUN echo "\ndaemon off;" >> /etc/nginx/nginx.conf
RUN chown -R www-data:www-data /var/lib/nginx
VOLUME ["/etc/nginx/sites-enabled", "/etc/nginx/certs", "/var/log/nginx"]

RUN rm /etc/nginx/sites-available/default 
RUN rm /etc/nginx/sites-enabled/default 
RUN cp default /etc/nginx/sites-available/default 
RUN ln -sfn /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default
RUN pip3 install -U scikit-image && pip3 install -U cython && pip3 install "git+https://github.com/philferriere/cocoapi.git#egg=pycocotools&subdirectory=PythonAPI"
RUN pip3 install -r requirements.txt 

RUN pip3 install http://download.pytorch.org/whl/cpu/torch-0.4.0-cp35-cp35m-linux_x86_64.whl && pip3 install torchvision

ADD supervisord.conf /etc/supervisor/conf.d/supervisord.conf
RUN sed -i '/daemon off;/c\daemon on;' /etc/nginx/nginx.conf
EXPOSE  80 443

ENV PYTHONPATH /app
ENTRYPOINT ["/usr/bin/supervisord"]

